//1. Find all Web Developers. ( It could be Web Developer III or Web Developer II or anything else )
function findAllpeopleByJob (dataset, wantedJob ) {
    const persons = [];
    if(Array.isArray(dataset) && typeof wantedJob == 'string') {
        for ( person of dataset ) {
            if( person.job.includes(wantedJob)) {
                persons.push(person);
            }
        }
        if(persons.length == 0 ) {
            throw new Error(`No one found with ${wantedJob} job.`);
        }
        return persons;
    } else {
        throw new Error('Arguments passed to function findAllpeopleByJob  is/are not valid' );
    }

}

module.exports = findAllpeopleByJob;