//2. Convert all the salary values into proper numbers instead of strings.

function convertSalaryValuesToNumbers (dataset) {
    if(Array.isArray(dataset)) {
        for(person of dataset) {
            person.salary = Number(person.salary.slice(1));
        }         
    } else {
        throw new Error('Arguemts passed to the function convertSalaryValuesToNumbers is not valid.')
    }
}

module.exports = convertSalaryValuesToNumbers;