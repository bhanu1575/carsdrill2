//    4. Find the sum of all salaries.

const updateSalary = require('./problem3.js')
function findSumOfAllSalaries (dataset) {
    if(Array.isArray(dataset)){
        updateSalary(dataset);
        let sumOfAllSalaries = 0;
        for (person of dataset ) {
            if(person.hasOwnProperty('updated_salary')){
                sumOfAllSalaries += person.updated_salary;
            }
        }
        return sumOfAllSalaries;
    } else {
        throw new Error('Arguments passed to the function sumOfAllSalaries is not valid.')
    }
}

module.exports = findSumOfAllSalaries;