//3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)

const convertSalaryValuesToStrings = require('./problem2.js')
function updateSalary(dataset,factor=10000) {
    if(Array.isArray(dataset) && typeof factor == 'number'){
        convertSalaryValuesToStrings(dataset)
        for( person of dataset) {
            person.updated_salary = person.salary*factor;
        }
    } else {
        throw new Error('Arguments passed to the function updateSalary is/are not valid')
    }
}

module.exports = updateSalary;