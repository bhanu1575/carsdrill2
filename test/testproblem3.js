//    3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)

let dataset = require('../js_drill_2.cjs');
const updateSalary = require('../problem3.js')

let factor = 10000;
try {
    updateSalary(dataset,factor)
    console.log(dataset)
} catch (error) {
    console.error(error.message);
}