// 6. Find the average salary of based on country. ( Groupd it based on country and then find the average ).

let dataset = require('../js_drill_2.cjs');
const getAvgSalaryOfAllCountries = require('../problem6.js');

try {
    let avgSalariesOfAllCountries = getAvgSalaryOfAllCountries(dataset);
    console.log(avgSalariesOfAllCountries);
} catch (error) {
    console.error(error.message);
}