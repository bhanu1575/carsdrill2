//5. Find the sum of all salaries based on country. ( Group it based on country and then find the sum ).

let dataset = require('../js_drill_2.cjs');
const getSalariesByCountry = require('../problem5.js');

try {
    let salariesByCountry = getSalariesByCountry(dataset);
    console.log(salariesByCountry);
} catch (error) {
    console.error(error.message);
}