//2. Convert all the salary values into proper numbers instead of strings.

let  dataset= require('../js_drill_2.cjs');
const convertSalaryValuesToNumbers = require('../problem2.js');

try {
    convertSalaryValuesToNumbers(dataset);
    console.log(dataset);
} catch (error) {
    console.error(error.message);
}