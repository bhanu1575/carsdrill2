//    4. Find the sum of all salaries.

const dataset = require('../js_drill_2.cjs');
const findSumOfAllSalaries = require('../problem4.js');

try {
    let sumOfAllSalaries = findSumOfAllSalaries(dataset);
    console.log(`sum of all salaries: ${sumOfAllSalaries}`);
} catch (error) {
    console.error(error.message);
}