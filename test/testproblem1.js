//1. Find all Web Developers. ( It could be Web Developer III or Web Developer II or anything else )

const dataset = require('../js_drill_2.cjs');
const findAllpeopleByJob = require('../problem1.js');
let wantedJob = 'Web Developer';

try {
    const persons = findAllpeopleByJob(dataset,wantedJob);
    for(person of persons) {
        console.log(JSON.stringify(person))
    }
} catch (error) {
    console.error(error.message);
}