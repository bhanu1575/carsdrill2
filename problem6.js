// 6. Find the average salary of based on country. ( Groupd it based on country and then find the average ).

const updateSalary = require('./problem3.js');


function getAvgSalariesOfAllCountries(dataset) {
    if(Array.isArray(dataset)) {
        updateSalary(dataset);
        let avgSalariesOfAllCountries = {};
        for(person of dataset) {
            if(avgSalariesOfAllCountries.hasOwnProperty(person.location)) {
                let totalSalary = ( avgSalariesOfAllCountries[person.location].avg_salary * avgSalariesOfAllCountries[person.location].count )
                let newTotalSalary = totalSalary  + person.updated_salary ;
                let totalCount = avgSalariesOfAllCountries[person.location].count + 1;
                avgSalariesOfAllCountries[person.location].avg_salary = newTotalSalary/totalCount;
                avgSalariesOfAllCountries[person.location].count = totalCount;

            } else {
                avgSalariesOfAllCountries[person.location] = {'avg_salary':person.updated_salary, 'count': 1};
            }
        }
        let updatedAvgSalaries = {};
        for( country in avgSalariesOfAllCountries) {
            updatedAvgSalaries[country] = avgSalariesOfAllCountries[country].avg_salary;
        }
        return updatedAvgSalaries;

    } else {
        throw new Error('Arguments passed to the function getAvgSalaryOfAllCountries is not valid type');
    }
}

module.exports = getAvgSalariesOfAllCountries;